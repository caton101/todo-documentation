# To-Do List Documentation

This repository hosts documentation for my [To-Do List program](https://gitlab.com/caton101/todo-list). The site is written in Markdown, built using Hugo, and deployed with GitLab Pages.

[Click here](https://caton101.gitlab.io/todo-documentation/) to see the website in action.

## Dependencies

- Git
- Hugo

## Building

1. Clone this repository:
`git clone https://gitlab.com/caton101/todo-documentation`

2. Enter the downloaded directory:
`cd todo-documentation`

3. Update the submodule(s):
`git submodule update --init`

4. Enter the website directory:
`cd docs`

5. Build the website:
`hugo`

## FAQ

**What version of Hugo do I use?**

I designed this site using the latest version of Hugo. At the time of writing, the site was built using Hugo `v0.76.5`.

**Can I contribute to the documentation?**

Yes. Check out the contributing section of this README.

**Why is the documentation separate from the program?**

The [KISS principle](https://en.wikipedia.org/wiki/KISS_principle) explains this better than this README could ever wish. Without going into too much detail, small isolated repositories help reduce the learning curve and make the project more accessible to potential contributors.

**Can I test the website without building it?**

Yes. Run the `hugo server` command inside the `./docs` directory.

## Troubleshooting

Follow the build instructions listed above. If this readme does not help you, please [create an issue](https://gitlab.com/caton101/todo-documentation/-/issues).

## Contributing

This project welcomes anyone who wishes to upgrade the documentation. Just clone the repository and make any changes you desire. When finished, please submit a pull request. Check read [this section](https://caton101.gitlab.io/todo-documentation/contributing/#writing-documentation) for more information.

## Licensing

This repository is licensed under the BSD 2-Clause License. For more info, please read the `LICENSE` file or visit [this link](https://opensource.org/licenses/BSD-2-Clause).
