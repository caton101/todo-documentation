---
title: "Usage"
date: 2020-10-23T13:46:14-04:00
draft: false
weight: 4
hide: footer
---

## Launching

1. Open a terminal session
2. Run the `todo` command

{{< asciinema videoID="asciicast-367803" videoSRC="https://asciinema.org/a/367803.js" >}}

## Exiting

1. Press the `ESCAPE` key

{{< asciinema videoID="asciicast-367817" videoSRC="https://asciinema.org/a/367817.js" >}}

## Keybindings

| Key          | Description                            |
| :----------- | :------------------------------------- |
| `UP_ARROW`   | Highlight the above task               |
| `DOWN_ARROW` | Highlight the below task               |
| `DELETE`     | Remove a task from the list            |
| `TAB`        | Toggle the completion status of a task |
| `ENTER`      | Select the highlighted element         |
| `ESCAPE`     | Exit the program                       |

## Add a task

1. Select the `[ADD TASK]` button
2. Enter the task's name (example: `get a haircut`)
3. Enter the task's location (example: `barbershop`)
4. Enter the task's date (example: `OCT-24-2020`)

{{< asciinema videoID="asciicast-367793" videoSRC="https://asciinema.org/a/367793.js" >}}

## Complete a task

1. Highlight a task
2. Press the `TAB` key

{{< asciinema videoID="asciicast-367794" videoSRC="https://asciinema.org/a/367794.js" >}}

## Remove a task

1. Highlight a task
2. Press the `DELETE` key

{{< asciinema videoID="asciicast-367795" videoSRC="https://asciinema.org/a/367795.js" >}}

## Edit a task

1. Highlight a task
2. Press the `ENTER` key
3. Follow the directions for the field you want to edit
    - [Change the name](#change-the-name)
    - [Change the location](#change-the-location)
    - [Change the date](#change-the-date)
    - [Delete the task](#delete-the-task) (legacy)

{{< asciinema videoID="asciicast-367800" videoSRC="https://asciinema.org/a/367800.js" >}}

### Change the name

1. Highlight the `Change name` option
2. Press `ENTER`
3. Enter the new name

{{< asciinema videoID="asciicast-367801" videoSRC="https://asciinema.org/a/367801.js" >}}

### Change the location

1. Highlight the `Change location` option
2. Press `ENTER`
3. Enter the new location

{{< asciinema videoID="asciicast-367799" videoSRC="https://asciinema.org/a/367799.js" >}}

### Change the date

1. Highlight the `Change date` option
2. Press `ENTER`
3. Enter the new date

{{< asciinema videoID="asciicast-367797" videoSRC="https://asciinema.org/a/367797.js" >}}

### Delete the task

**Note:**
This is a legacy feature and may be removed in future versions.

1. Highlight the `Delete` option
2. Press `ENTER`

{{< asciinema videoID="asciicast-367798" videoSRC="https://asciinema.org/a/367798.js" >}}

### Cancel edits

1. Highlight the `Cancel` option
2. Press `ENTER`

{{< asciinema videoID="asciicast-367796" videoSRC="https://asciinema.org/a/367796.js" >}}
