---
title: "Getting Started"
date: 2020-10-23T16:05:40-04:00
draft: false
weight: 2
hide: footer
---

## How to install

Follow the directions listed [here](/configuration/#install).

**Note:**
Only Linux is officially supported.

**Note:**
For other operating systems, see these instructions:

- [Install on Windows](/faq/#can-i-use-this-program-on-windows).
- [Install on macOS](/faq/#can-i-use-this-program-on-macos).
- [Install on alternate operating system](/faq/#can-i-use-this-program-on-random-operating-system-instead-of-linux).

## How to use the program

Read the [usage section](/usage).

## How to troubleshoot

Read the documentation **first** for troubleshooting and questions. If the documentation does not help you, please [create an issue](https://gitlab.com/caton101/todo-list/-/issues).

## How to contribute

You can contribute in several ways:

- [Modify the source code](http://localhost:1313/todo-documentation/contributing/#writing-code)
- [Modify the documentation](http://localhost:1313/todo-documentation/contributing/#writing-documentation)
- [Share this program with others](http://localhost:1313/todo-documentation/contributing/#sharing-with-others)
